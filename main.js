// This is an active module of the noinhoquehuong (1) Add-on
/**
 * create all variables and functions in BelllOfMindfulnessMain
 */

if (typeof BelllOfMindfulnessMain == "undefined") {
    var BelllOfMindfulnessMain = {
		timer					: 	0,
		time_space_choices		: 	[5, 10, 15, 20, 30, 45, 60],
		time_space				: 	null,
		time_str				: 	"",
		bell0timer				:	null,
		bell1timer				:	null,
		bell2timer				:	null,
		bell3timer				:	null,
		isdebug 				: 	false,
		isBGPageReady 			: 	false,
		tmr  			        : 	require("sdk/timers"),
		simprefs				: 	require("sdk/simple-prefs"),
		data					:	require("sdk/self").data,
		panels					:	require("sdk/panel"),
		notifications			:	require("sdk/notifications"),
		pageWorkers				:   require("sdk/page-worker"),
        buttons                 :   require('sdk/ui/button/action'),
		_ 						:	require("sdk/l10n").get,
		Panel					:	null,
		BGPage					:	null,
        Button                  :   null,
        ButtonClick             :   null,
        ButtonOnState           :   null,
        ButtonOffState          :   null,
		IconURL					:   null,
		Notification			:	null,
		emitInviteBell			:	null,
		updateTime				:	null,
		resetTime				:	null,
        writeTime               :   null,
	};
	BelllOfMindfulnessMain.BGPage = BelllOfMindfulnessMain.pageWorkers.Page({
		contentURL: BelllOfMindfulnessMain.data.url("BellOfMindfulnessBackground.html"),
		contentScriptWhen: "ready",
		contentScriptFile: BelllOfMindfulnessMain.data.url("BellOfMindfulnessBackground.js"),
		onMessage: function(message) {
			console.log(message);
		}
	});
	BelllOfMindfulnessMain.Panel = BelllOfMindfulnessMain.panels.Panel({
		width:550,
		height:600,
		contentURL: BelllOfMindfulnessMain.data.url("BellOfMindfulnessPanel.html"),
		contentScriptWhen: "ready",
		contentScriptFile: BelllOfMindfulnessMain.data.url("BellOfMindfulnessPanel.js")
	});
    BelllOfMindfulnessMain.ButtonClick = function(state){
        BelllOfMindfulnessMain.Panel.show({position:BelllOfMindfulnessMain.Button});
    };
    BelllOfMindfulnessMain.Button = BelllOfMindfulnessMain.buttons.ActionButton({
        id: "bell-of-mindfulness",
        label: "Bell of Mindfulness",
        icon: BelllOfMindfulnessMain.data.url("icon.png"),
        onClick: BelllOfMindfulnessMain.ButtonClick
    });
    BelllOfMindfulnessMain.ButtonOnState = {
        label: "On",
        icon: BelllOfMindfulnessMain.data.url("icon.png")
    };
    BelllOfMindfulnessMain.ButtonOffState = {
        label: "Off",
        icon: BelllOfMindfulnessMain.data.url("icon-grayscale.png")
    };
    
	BelllOfMindfulnessMain.IconURL = BelllOfMindfulnessMain.data.url("icon.png");
	BelllOfMindfulnessMain.Notification = {
	  	title: BelllOfMindfulnessMain._("Notification_Title"),
	  	text: "\n" + BelllOfMindfulnessMain._("Notification_TextBreatheIn")+"\n"+BelllOfMindfulnessMain._("Notification_TextBreatheOut") + "\n",
	  	iconURL: BelllOfMindfulnessMain.IconURL
	};
	// send to invite the bell
	BelllOfMindfulnessMain.emitInviteBell = function(){
		BelllOfMindfulnessMain.tmr.clearTimeout(BelllOfMindfulnessMain.bell0timer);
		BelllOfMindfulnessMain.tmr.clearTimeout(BelllOfMindfulnessMain.bell1timer);
		BelllOfMindfulnessMain.tmr.clearTimeout(BelllOfMindfulnessMain.bell2timer);
		BelllOfMindfulnessMain.tmr.clearTimeout(BelllOfMindfulnessMain.bell3timer);
		if (BelllOfMindfulnessMain.isBGPageReady && BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled){
			
			BelllOfMindfulnessMain.bell0timer = BelllOfMindfulnessMain.tmr.setTimeout(function(){
				BelllOfMindfulnessMain.BGPage.port.emit("nhatchuong");
			},500);
			
			BelllOfMindfulnessMain.bell1timer = BelllOfMindfulnessMain.tmr.setTimeout(function(){
				BelllOfMindfulnessMain.BGPage.port.emit("thinhchuong");
			},5000);
			
			BelllOfMindfulnessMain.bell2timer = BelllOfMindfulnessMain.tmr.setTimeout(function(){
				BelllOfMindfulnessMain.BGPage.port.emit("thinhchuong");
			},15000);
			
			BelllOfMindfulnessMain.bell3timer = BelllOfMindfulnessMain.tmr.setTimeout(function(){
				BelllOfMindfulnessMain.BGPage.port.emit("thinhchuong");
			},25000);
			
			BelllOfMindfulnessMain.notifications.notify(BelllOfMindfulnessMain.Notification);
		}
	
	}
    BelllOfMindfulnessMain.writeTime = function(timer){
		var timestr = "";
		var tempstr = "";        
        if (Math.floor(timer/60)>0){ // display only the minute
            timestr += "" + Math.floor(timer/60);
            if (timestr.length == 1) timestr = "0" + timestr;
            timestr += "'";
        }else{
            timestr += timer%60;
            if (timestr.length == 1) timestr = "0" + timestr;
        }
		BelllOfMindfulnessMain.Button.badge = timestr;
    };
    
	// recursive method to update the time
	BelllOfMindfulnessMain.updateTime = function(){
		
		if (BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled){
			BelllOfMindfulnessMain.timer--;
			if(BelllOfMindfulnessMain.timer<=0){
				BelllOfMindfulnessMain.emitInviteBell();
				BelllOfMindfulnessMain.resetTime();
			}
			BelllOfMindfulnessMain.Panel.port.emit("setTime",BelllOfMindfulnessMain.timer);
            BelllOfMindfulnessMain.writeTime(BelllOfMindfulnessMain.timer);
		}
		else {
			BelllOfMindfulnessMain.timer = 0;
            BelllOfMindfulnessMain.Button.badge = "OFF";
		}
		BelllOfMindfulnessMain.tmr.setTimeout(BelllOfMindfulnessMain.updateTime,1000);
	}
	
	BelllOfMindfulnessMain.resetTime = function(){
		BelllOfMindfulnessMain.timer = BelllOfMindfulnessMain.time_space*60;
	}
	
	exports.main = function() {
		//just the initial values
		/*if (simprefs.prefs.timeSpaceOption == null){
			simsimprefs.prefs.timeSpaceOption = 2;
			if(isdebug){console.log("BOM_Main: Initialized timeSpaceOption");}
		}
		
		if (simsimprefs.prefs.volume==null){
			simsimprefs.prefs.volume = 30;
			if(isdebug){console.log("BOM_Main: Initialized volume");}
		}
		
		if (simsimprefs.prefs.isBellEnabledd == null){
			simsimprefs.prefs.isBellEnabledd = true;
			if(isdebug){console.log("BOM_Main: Initialized isBellEnabled");}
		}*/
		
		if (BelllOfMindfulnessMain.simprefs.prefs.volume >100){
			BelllOfMindfulnessMain.simprefs.prefs.volume = 100;
		}
		if (BelllOfMindfulnessMain.simprefs.prefs.volume <0){
			BelllOfMindfulnessMain.simprefs.prefs.volume = 0;
		}
		
		//initiallizing values
		BelllOfMindfulnessMain.timer = 0;
		BelllOfMindfulnessMain.time_space = BelllOfMindfulnessMain.time_space_choices[BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption];
		if(BelllOfMindfulnessMain.isdebug){console.log("BOM_Main: The bell is ENABLED = " + BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled);}
		var initData = {
			timeSpaceOption: BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption,
			volume: BelllOfMindfulnessMain.simprefs.prefs.volume,
			isBellEnabled: BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled,
			bellOffValue: BelllOfMindfulnessMain._("OFF_txt"),
			bellOnValue: BelllOfMindfulnessMain._("ON_txt"),
			inviteBellButtonValue: BelllOfMindfulnessMain._("InviteBell_input")
		};
		//set listeners so that the panels and preferences can call
		BelllOfMindfulnessMain.BGPage.port.on("BGPageReady", function() {
			BelllOfMindfulnessMain.isBGPageReady = true;
			BelllOfMindfulnessMain.tmr.setTimeout(BelllOfMindfulnessMain.updateTime,1000);
		});
		
		BelllOfMindfulnessMain.BGPage.port.on("getInit", function() {
			BelllOfMindfulnessMain.BGPage.port.emit("setInit",initData);
		});
						
		BelllOfMindfulnessMain.Panel.port.on("changeVolume", function(volume) {
			volume = parseInt(volume);
			if (volume < 0) { volume = 0;}
			if (volume>100) { volume = 100;}
			BelllOfMindfulnessMain.simprefs.prefs.volume = volume;
		});
				
		BelllOfMindfulnessMain.Panel.port.on("changeTimeSpaceOption", function(timeSpaceOption) {
			if (BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption == timeSpaceOption){
				return;
			}
			BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption = timeSpaceOption;
		});
				
		BelllOfMindfulnessMain.Panel.port.on("changeBellEnable",function(BellEnableState){
			if (BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled != BellEnableState){
				BelllOfMindfulnessMain.timer = 0;
				BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled = BellEnableState;
			}
		});
				
		BelllOfMindfulnessMain.Panel.port.on("inviteBell",function(){
			BelllOfMindfulnessMain.emitInviteBell();
		});
				
		BelllOfMindfulnessMain.Panel.port.on("getInit", function() {
			BelllOfMindfulnessMain.Panel.port.emit("setInit",initData);
		});
				
		// on the change of preferences
		BelllOfMindfulnessMain.simprefs.on("timeSpaceOption",function(prefName){
			if(BelllOfMindfulnessMain.isdebug){console.log('BOM_Main: TimeSpaceOption Preference is changed to option ' + BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption);}
			if (BelllOfMindfulnessMain.timer>BelllOfMindfulnessMain.time_space_choices[BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption]*60){
				BelllOfMindfulnessMain.timer -= (BelllOfMindfulnessMain.time_space-BelllOfMindfulnessMain.time_space_choices[BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption])*60;
			}
			BelllOfMindfulnessMain.time_space = BelllOfMindfulnessMain.time_space_choices[BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption];
			BelllOfMindfulnessMain.Panel.port.emit("changeTimeSpace",BelllOfMindfulnessMain.simprefs.prefs.timeSpaceOption);
		});
		
		BelllOfMindfulnessMain.simprefs.on("isBellEnabled",function(prefName){
			if(BelllOfMindfulnessMain.isdebug){console.log('BOM_Main: Bell is Enabled/Disabled '+BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled);}
			BelllOfMindfulnessMain.BGPage.port.emit("changeBellEnable",BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled);
			BelllOfMindfulnessMain.Panel.port.emit("changeBellEnableState",BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled);
            if (BelllOfMindfulnessMain.simprefs.prefs.isBellEnabled){
                BelllOfMindfulnessMain.Button.state("window",BelllOfMindfulnessMain.ButtonOnState);
                BelllOfMindfulnessMain.Button.badge = null;
                BelllOfMindfulnessMain.Button.badgeColor = "#e56524";
            }else{
                BelllOfMindfulnessMain.Button.state("window",BelllOfMindfulnessMain.ButtonOffState);
                BelllOfMindfulnessMain.Button.badgeColor = "#000000";
                BelllOfMindfulnessMain.Button.badge = null;
            }
		});
		
		BelllOfMindfulnessMain.simprefs.on("volume",function(prefName){
			if(BelllOfMindfulnessMain.isdebug){console.log('BOM_Main: volume pref change');}
			if (BelllOfMindfulnessMain.simprefs.prefs.volume < 0) { BelllOfMindfulnessMain.simprefs.prefs.volume = 0; return;}
			if (BelllOfMindfulnessMain.simprefs.prefs.volume>100) { BelllOfMindfulnessMain.simprefs.prefs.volume = 100; return;}
			BelllOfMindfulnessMain.BGPage.port.emit("changeVolume",BelllOfMindfulnessMain.simprefs.prefs.volume);
			BelllOfMindfulnessMain.Panel.port.emit("changeVolumeValue",BelllOfMindfulnessMain.simprefs.prefs.volume);
		});
		
		BelllOfMindfulnessMain.simprefs.on("inviteBell",function(prefName){
			if(BelllOfMindfulnessMain.isdebug){console.log('BOM_Main: inviting bell from pref panel!');}
			BelllOfMindfulnessMain.emitInviteBell();
		});
	};
};




