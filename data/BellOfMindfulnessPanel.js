if (typeof BelllOfMindfulnessPanel == "undefined") {
	BelllOfMindfulnessPanel = {
		isBellEnabled				:	true,
		bellOffValue				:	null,
		bellOnValue					:	null,
		timeLeftBox					:	null,
		isBellEnabledSwitch			:	null,
		onLayer						:	null,
		offLayer					:	null,
		timespaceSelection			:	null,
		volumeSetter				:	null,
		volumeIndicator				:	null,
		inviteBellButton			:	null,
		thichNhatHanh				:	null,
		isdebug						:	false,
		setBellEnable				:	null,
		inviteBell					:	null,
		changeVolume				:	null,
		writeTime					:	null,
		changeTimeSpace				: 	null,
		initBellOfMindfulnessPanel	: 	null
	}
	
	BelllOfMindfulnessPanel.setBellEnable = function(){
		BelllOfMindfulnessPanel.isBellEnabled = BelllOfMindfulnessPanel.isBellEnabledSwitch.checked;
		if (BelllOfMindfulnessPanel.isBellEnabled){
			BelllOfMindfulnessPanel.onLayer.style.display = "block";
			BelllOfMindfulnessPanel.offLayer.style.display = "none";
		}
		else{
			BelllOfMindfulnessPanel.offLayer.style.display = "block";
			BelllOfMindfulnessPanel.onLayer.style.display = "none";
		}
		self.port.emit("changeBellEnable",BelllOfMindfulnessPanel.isBellEnabled);
		if (BelllOfMindfulnessPanel.isdebug) {console.log("BOMPanel: isBellEnabled: " + BelllOfMindfulnessPanel.isBellEnabled)};
	}
	
	BelllOfMindfulnessPanel.inviteBell = function(){
		self.port.emit("inviteBell");
	}
	
	BelllOfMindfulnessPanel.changeVolume = function(){
		BelllOfMindfulnessPanel.volumeIndicator.value = '' + BelllOfMindfulnessPanel.volumeSetter.value + '%';
		self.port.emit("changeVolume",BelllOfMindfulnessPanel.volumeSetter.value);
	}
	
	BelllOfMindfulnessPanel.writeTime = function(timer){
		var timestr = '';
		var tempstr = '';
		timestr += ''+ Math.floor(timer/60);
		if (timestr.length == 1) timestr = '0' + timestr;
		timestr += ':';
		tempstr += timer%60;
		if (tempstr.length == 1) tempstr = '0' + tempstr;
		timestr += tempstr;
		
		BelllOfMindfulnessPanel.timeLeftBox.value = timestr;
	}
	
	BelllOfMindfulnessPanel.changeTimeSpace = function(){
		self.port.emit("changeTimeSpaceOption", BelllOfMindfulnessPanel.timespaceSelection.selectedIndex);
	}
	BelllOfMindfulnessPanel.initBellOfMindfulnessPanel = function(){
		
		BelllOfMindfulnessPanel.timeLeftBox = document.getElementById("timeleft");
		BelllOfMindfulnessPanel.isBellEnabledSwitch = document.getElementById("isBellEnabledSwitch");
		BelllOfMindfulnessPanel.onLayer = document.getElementById("onLayer");
		BelllOfMindfulnessPanel.offLayer = document.getElementById("offLayer");
		BelllOfMindfulnessPanel.timespaceSelection = document.getElementById("timespace");
		BelllOfMindfulnessPanel.volumeSetter = document.getElementById("volume");
		BelllOfMindfulnessPanel.volumeIndicator = document.getElementById("volume_txt");
		BelllOfMindfulnessPanel.inviteBellButton = document.getElementById("invitebell");
		BelllOfMindfulnessPanel.thichNhatHanh = document.getElementById("thichNhatHanh");
		
		self.port.once("setInit", function(initData){
			if (BelllOfMindfulnessPanel.isdebug) {console.log("BOMPanel: initData!")};
			BelllOfMindfulnessPanel.bellOffValue = initData.bellOffValue;
			BelllOfMindfulnessPanel.bellOnValue = initData.bellOnValue;
			BelllOfMindfulnessPanel.isBellEnabled = initData.isBellEnabled;
			BelllOfMindfulnessPanel.timespaceSelection.selectedIndex = initData.timeSpaceOption;
			BelllOfMindfulnessPanel.volumeSetter.value = initData.volume;
			BelllOfMindfulnessPanel.volumeIndicator.value = '' + BelllOfMindfulnessPanel.volumeSetter.value + '%';
			BelllOfMindfulnessPanel.isBellEnabledSwitch.checked = BelllOfMindfulnessPanel.isBellEnabled;
			BelllOfMindfulnessPanel.volumeSetter.onclick = BelllOfMindfulnessPanel.changeVolume;
			BelllOfMindfulnessPanel.timespaceSelection.onclick = BelllOfMindfulnessPanel.changeTimeSpace;
			BelllOfMindfulnessPanel.inviteBellButton.onclick = BelllOfMindfulnessPanel.inviteBell;
			BelllOfMindfulnessPanel.thichNhatHanh.onclick = BelllOfMindfulnessPanel.inviteBell;
			BelllOfMindfulnessPanel.isBellEnabledSwitch.onclick = BelllOfMindfulnessPanel.setBellEnable;
			BelllOfMindfulnessPanel.offLayer.style.height = BelllOfMindfulnessPanel.onLayer.offsetHeight;
			
			BelllOfMindfulnessPanel.setBellEnable();
			
			var newStyle = '<style>\n';
                newStyle += '.onoffswitch-inner:before {\n';
                newStyle += 'content: "' + BelllOfMindfulnessPanel.bellOnValue + '";\n';
                newStyle += '}\n';
                newStyle += '.onoffswitch-inner:after {\n'
                newStyle += 'content: "' + BelllOfMindfulnessPanel.bellOffValue + '";\n'
                newStyle += '}</style>';
			document.getElementById("BellOfMindfulness_head").innerHTML += newStyle;
			
			BelllOfMindfulnessPanel.inviteBellButton.value = initData.inviteBellButtonValue;
		});
		
		self.port.on("setTime",function(timer){
			BelllOfMindfulnessPanel.writeTime(timer);
		});
		
		self.port.on("changeTimeSpace",function(timeSpaceOption){
			BelllOfMindfulnessPanel.timespaceSelection.selectedIndex = timeSpaceOption;
		});
		self.port.on("changeVolumeValue",function(volume){
			BelllOfMindfulnessPanel.volumeSetter.value = volume;
			BelllOfMindfulnessPanel.volumeIndicator.value = '' + BelllOfMindfulnessPanel.volumeSetter.value + '%';
			if (BelllOfMindfulnessPanel.isdebug) {console.log("BOMPanel: changeVolume: " + volume)};
		});
		
		self.port.on("changeBellEnableState",function(isBellEnableState){
			BelllOfMindfulnessPanel.isBellEnabledSwitch.checked = isBellEnableState;
			BelllOfMindfulnessPanel.isBellEnabled = isBellEnableState;
			if (BelllOfMindfulnessPanel.isBellEnabled){
				BelllOfMindfulnessPanel.onLayer.style.display = "block";
				BelllOfMindfulnessPanel.offLayer.style.display = "none";
			}
			else{
				BelllOfMindfulnessPanel.offLayer.style.display = "block";
				BelllOfMindfulnessPanel.onLayer.style.display = "none";
			}
			if (BelllOfMindfulnessPanel.isdebug) {console.log("BOMPanel: isBellEnabled: " + BelllOfMindfulnessPanel.isBellEnabled)};
		});
		
		window.onunload = function(){
			BelllOfMindfulnessPanel = undefined;
		}
		
		self.port.emit("getInit");
		self.port.emit("panelReady");
	}
	BelllOfMindfulnessPanel.initBellOfMindfulnessPanel();
}