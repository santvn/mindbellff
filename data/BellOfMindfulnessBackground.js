if (typeof BelllOfMindfulnessBG == "undefined") {
	var BelllOfMindfulnessBG = {
		isBellEnabled			:	true,
		nhatChuongBell			:	null,
		thinhChuongBell			:	null,
		isdebug					:	false,
		changeVolume			: 	null,
		initBellOfMindfulnessWidget	: null,
		
	}
		
	BelllOfMindfulnessBG.changeVolume = function(volume){
		BelllOfMindfulnessBG.nhatChuongBell.volume = volume/100;
		BelllOfMindfulnessBG.thinhChuongBell.volume = volume/100;
		if (BelllOfMindfulnessBG.isdebug) {console.log("BOMBGPage: changeVolume: " + volume)};
	}
	BelllOfMindfulnessBG.initBellOfMindfulnessWidget = function(){
		
		BelllOfMindfulnessBG.nhatChuongBell = document.getElementById("nhatChuong");
		BelllOfMindfulnessBG.thinhChuongBell = document.getElementById("thinhChuong");
		
		self.port.once("setInit", function(initData){
			BelllOfMindfulnessBG.changeVolume(initData.volume);
			BelllOfMindfulnessBG.isBellEnabled = initData.isBellEnabled;
		});
		
		self.port.on("nhatchuong",function(){
			if (BelllOfMindfulnessBG.isBellEnabled){
				BelllOfMindfulnessBG.nhatChuongBell.pause();
				BelllOfMindfulnessBG.thinhChuongBell.pause();
				BelllOfMindfulnessBG.nhatChuongBell.currentTime=0;
				BelllOfMindfulnessBG.thinhChuongBell.currentTime=0;
				BelllOfMindfulnessBG.nhatChuongBell.play();
				if (BelllOfMindfulnessBG.isdebug) {console.log("BOMBGPage: nhatchuong")};
			}
		});
		
		self.port.on("thinhchuong",function(){
			if (BelllOfMindfulnessBG.isBellEnabled){
				BelllOfMindfulnessBG.nhatChuongBell.pause();
				BelllOfMindfulnessBG.thinhChuongBell.pause();
				BelllOfMindfulnessBG.nhatChuongBell.currentTime=0;
				BelllOfMindfulnessBG.thinhChuongBell.currentTime=0;
				BelllOfMindfulnessBG.thinhChuongBell.play();
				if (BelllOfMindfulnessBG.isdebug) {console.log("BOMBGPage: thinhchuong")};
			}
		});
		
		self.port.on("changeVolume",function(volume){
			BelllOfMindfulnessBG.changeVolume(volume);
		});
		
		self.port.on("changeBellEnable",function(BellEnableState){
		   BelllOfMindfulnessBG.isBellEnabled = BellEnableState;
		});
		
		self.port.emit("getInit");
		self.port.emit("BGPageReady");
	}
	BelllOfMindfulnessBG.initBellOfMindfulnessWidget();
}